
from argparse import ArgumentParser
from os.path import join as join_path
from glob import glob
import re
import logging
import subprocess
import time
import configuration
from os import environ
import os
logging.basicConfig(level=logging.INFO, format="%(asctime)s %(levelname)s - %(message)s")

USE_SLURM = False
def parse_args():
    parser = ArgumentParser("AARTFAAC imaging pipeline")
    parser.add_argument("input_visibilities")
    parser.add_argument('--antenna_set', default='A12', choices=list(configuration.ANTENNA_SETS.keys()))
    parser.add_argument('--filter', help='filter files to process')
    parser.add_argument('--process-only', action='store_true')
    parser.add_argument('--join', action='store_true')
    parser.add_argument("workdir")
    return parser.parse_args()

def run_command_locally(cmd):
    logging.info("Trying executing command %s", cmd)
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    c=process.poll()
    while c == None:
        time.sleep(1)
        c = process.poll()
    stdout, stderr = process.communicate()
    stdout = stdout.decode()
    stderr = stderr.decode()
    if c == 0:
        logging.info("Command executed with stdout: %s and stderr: %s", stdout, stderr)
    else:
        logging.error("Command executed with stdout: %s and stderr: %s", stdout, stderr) 

def threaded_command_run(cmd, callback=None, callback_args=None, **kwargs):
    import threading
    envs = dict(environ)
    if kwargs != None:
        for key, value in kwargs.items():
            envs[key] = value
    
    def run_command_in_thread():
        for k in range(3):
            logging.info("Trying executing command %s for the %s time", cmd, k)
            process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True,env=envs)
            c=process.poll()
            while c == None:
                time.sleep(1)
                c = process.poll()
            stdout, stderr = process.communicate()
            stdout = stdout.decode()
            stderr = stderr.decode()
            if c == 0:
                logging.info("Command executed with stdout: %s and stderr: %s", stdout, stderr)
                if callback:
                    callback(*callback_args)
                return
            else:
                logging.error("Command executed with stdout: %s and stderr: %s", stdout, stderr) 
            
    thread = threading.Thread(target=run_command_in_thread)
    thread.start()

def step(step_function, *args, **kwargs):
    def wrapper(*args, **kwargs):
        logging.info("Executing step %s ", step_function.__name__)
        try: 
            step_function(*args, **kwargs)
        except Exception as e:
            logging.exception(e)
            logging.error("Failed step %s", step_function.__name__)
        logging.info("Step %s executed successfully", step_function.__name__)
    return wrapper

def find_files(path, pattern, parse_regex=None):
    files = glob(join_path(path, pattern))
    if parse_regex:
        return {re.search(parse_regex, file_name).groups(): file_name for file_name in files}
    else:
        return sorted(files)
    
@step
def multi_subband_image(workdir, input_dir, step=10, filter_str=None):
    if filter_str is None:
        filter_str = '*calibrated*.ms'
    path = os.path.join(input_dir,filter_str)
    input_mss = sorted(glob(path))
    workdir = os.path.join(workdir, 'joined')
    os.makedirs(workdir, exist_ok=True)
    subbands = [re.search('sb(\d{3})',os.path.basename(fname)).groups()[0] for fname in input_mss]
    for k in range(0, len(input_mss), step):
        start = k
        end = min(k + step, len(input_mss))
        block_input = input_mss[start: end]
        s_subband, e_subband = subbands[start], subbands[end-1]
        output = f'{workdir}/image_{s_subband}-{e_subband}'
        input_line = " ".join(block_input)
        command_noconv = f"wsclean -size 2300 2300 -scale 0.05 -no-update-model-required -pol I -niter 30 -name {output} -no-dirty -data-column DATA {input_line}"
        cmd = f'srun -c 10 -n 1 {command_noconv}'
        threaded_command_run(cmd)


@step 
def image(workdir, datasets):
    from configuration import IMAGE_OUTPUT_DIR
    outpath = join_path(workdir, IMAGE_OUTPUT_DIR)
    os.makedirs(outpath, exist_ok=True)

    def image_single(cmd, next_cmd=None):
        if next_cmd:
            if USE_SLURM:
                threaded_command_run(f"srun  -c 10 -n 1  {cmd}", callback=image_single, callback_args=[next_cmd])
            else:
                threaded_command_run(cmd, callback=image_single, callback_args=[next_cmd])
        else:
            if USE_SLURM:
                threaded_command_run(f"srun  -c 10 -n 1  {cmd}")
            else:
                threaded_command_run(cmd)
    for dataset in datasets:
        name = os.path.basename(dataset).replace(".ms", "").replace(".MS", "")
        os.makedirs(f"{outpath}/conv", exist_ok=True)
        os.makedirs(f"{outpath}/noconv", exist_ok=True)
        command_conv = f"wsclean -size 2300 2300 -scale 0.05 -no-update-model-required -pol I --intervals-out 4 -niter 0 -name {outpath}/noconv/{name} -no-dirty -data-column DATA {dataset}"
        command_noconv = f"wsclean -size 2300 2300 -scale 0.05 -no-update-model-required -pol I --intervals-out 4 -niter 30 -name {outpath}/conv/{name} -no-dirty -data-column DATA {dataset}"
        #image_single(command_conv, command_noconv)



@step
def process_data(workdir, correlated_files, antenna_conf_path=None):
    from configuration import PROCESSED_OUTPUT_DIR, DP3_PARSET, ANTENNA_CONFIGURATION
    
    if antenna_conf_path is None:
        antennaconf = join_path(workdir, ANTENNA_CONFIGURATION)
    else:
        antennaconf = join_path(antenna_conf_path, ANTENNA_CONFIGURATION)
    outpath = join_path(workdir, PROCESSED_OUTPUT_DIR)
    
    print('Using antenna conf', antennaconf)
    os.makedirs(outpath, exist_ok=True)
    for correlated_file in correlated_files:
        output_file_name = os.path.join(outpath, os.path.basename(correlated_file).replace('.vis', 'calibrated.ms'))
        raw_output_file_name = os.path.join(outpath, os.path.basename(correlated_file).replace('.vis', 'raw.ms'))
        
        command = f"/home/mancini/opt/dp3_devel/bin/DP3 {DP3_PARSET} msin={correlated_file} msout={output_file_name} msin.antennafield={antennaconf} " \
                  "msin.ntimesteps=4 " \
                  f"out.name={raw_output_file_name} "\
                  f"phasecal.h5parm={output_file_name.replace('.ms', '-')}phase.h5 " \
                  f"amplitudecal.parmdb={output_file_name.replace('.ms', '-')}amplitudes.h5 " \
                  f"ddecal.h5parm={output_file_name.replace('.ms', '-')}ddecal.h5"
        if USE_SLURM:
            threaded_command_run(f"srun -c 10 -n 1 {command}", callback=image, callback_args=[workdir, [output_file_name]])
        else:
            threaded_command_run(command, callback=image, callback_args=[workdir, [output_file_name]])
            


@step
def correlate_data(input_dir, workdir, antenna_set, filter_str=None):
    antennas = configuration.ANTENNA_SETS[antenna_set]
    from configuration import CORRELATED_OUTPUT_DIR, N_SUB_SUBBANDS
    
    outpath = join_path(workdir, CORRELATED_OUTPUT_DIR)
    os.makedirs(outpath, exist_ok=True)
    if filter_str is None:
        filter_str = "*.raw"
    input_files = find_files(input_dir, filter_str, "(CS\d{3}).*-sb(\d{3}).raw")
    stations, subbands = zip(*input_files.keys())
    unique_stations = sorted(set(stations))
    unique_subbands = sorted(set(subbands))
    assert len(set(antennas).difference(unique_stations)) == 0
    
    for subband in unique_subbands:
        logging.info("Processing subbands %s", subband)
        sub_input_files = [f"{input_dir}/{antenna}.raw-sb{subband}.raw" for antenna in antennas]
        subband_string = ",".join([f"{int(subband) + k:03d}" for k in range(N_SUB_SUBBANDS)])
        input_string = ",".join([f"file:{output_file}" for output_file in sub_input_files])
        output_files = [f"{outpath}/{antenna_set}-sb{int(subband) + s_subband:03d}.vis" for s_subband in range(N_SUB_SUBBANDS)]
        output_string = ",".join([f"file:{outfile}" for outfile in output_files])
        n_antennas = len(antennas) * 48 
        date = '2023-09-27 14:48:44'
        olddate= '2022-03-23 21:12:34'
        command = f"AARTFAAC -p1 -n{n_antennas} -t768 -c256 -C17 -b16 -s8 -m15 -D'{date}' -r4 -g0 -q1 -R0 -S{subband_string} -i {input_string} -o {output_string}"
        if USE_SLURM:
            threaded_command_run(f"srun -N1 --gres=gpu:1 -CA4000 {command}", callback=process_data, callback_args=[workdir, output_files], TZ="UTC")
        else:
            threaded_command_run(command, callback=process_data, callback_args=[workdir, output_files], TZ="UTC")
@step
def create_antenna_field(input_dir, workdir, antenna_set, single=False, filter_str=None):
    antennas = configuration.ANTENNA_SETS[antenna_set]
    output_file = f"{workdir}/{configuration.ANTENNA_CONFIGURATION}"
    cmd = f"create-antennafield.py -s {' '.join(antennas)} -o {output_file}"
    if single:
        threaded_command_run(cmd, callback_args=[input_dir, workdir, antenna_set, filter_str])
    else:
        threaded_command_run(cmd, callback=correlate_data, callback_args=[input_dir, workdir, antenna_set, filter_str])


def only_process(input_dir, workdir, filter_str=None):
    if filter_str:
        files = glob(f"{input_dir}/{filter_str}.vis")
    else:
        files = glob(f"{input_dir}/*.vis")
    
    process_data(workdir, files, antenna_conf_path=os.path.dirname(input_dir))

def main():
    args = parse_args()
    os.makedirs(args.workdir, exist_ok=True)

    if args.process_only:
        create_antenna_field('', args.input_visibilities, args.antenna_set, single=True, filter_str=args.filter)
        only_process(args.input_visibilities, args.workdir, args.filter)
    elif args.join:
        multi_subband_image(args.workdir, args.input_visibilities)
    else:
        create_antenna_field(args.input_visibilities, args.workdir, args.antenna_set, single=False, filter_str=args.filter)
    import threading
    while threading.activeCount() > 1:
        time.sleep(1)
        logging.info("Waiting for processes to finish")

if __name__ == "__main__":
    main()
