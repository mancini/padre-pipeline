#!/bin/bash

module load spack
module use $HOME/privatemodules
module load cuda fftw hdf5 boost casacore wsclean aartfaac/main dp3/devel aftool/master tbbconv/master tcc/cmake gsl parallel py-casacore py-ipython py-numpy
export OPENBLAS_NUM_THREADS=1
