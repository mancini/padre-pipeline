from argparse import ArgumentParser
import astropy.io.fits as fits
import numpy as np
from astropy.wcs import WCS

def parse_args():
    parser = ArgumentParser(description="Join single images in a fits cube")
    parser.add_argument('inputs', nargs='+')
    parser.add_argument('output')

    return parser.parse_args()


def convert(images, out):
    first_image = fits.open(images[0])[0]
    header, data = first_image.header, first_image.data
    wcs = WCS(header)
    print(wcs, dir(wcs))
    for image in images[1:]:
        hdu, *_ = fits.open(image)
        data= np.concatenate([data, hdu.data], axis=1)
        
    fits.writeto(out, data, header, overwrite=True)


def main():
    args = parse_args()
    convert(args.inputs, args.output)



if __name__ == '__main__':
    main()