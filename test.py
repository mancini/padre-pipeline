# coding: utf-8
from casacore.tables import table
import numpy as np
t = table("A21-sb206calibrated.ms")

f = t.getcol("FLAG")
print('Flagged data', f.sum())
print('Remaining nan count', np.isnan(t.getcol("DATA")[~f]).sum())
print('Remaining data', t.getcol("DATA")[~f].size)
print('Flagged count', np.isnan(t.getcol("DATA")[f]).sum())

